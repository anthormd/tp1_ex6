#!/usr/bin/env python
# coding: utf-8
"""fichier de test de la classe SimpleCalculator"""

from calculator.simple_calculator import SimpleCalculator
import unittest
"""
on importe les packages dans pythonpath

on importe la classe SimpleCalculator
"""

class TestSimpleCalculator(unittest.TestCase):
    """TestCase pour tester les méthodes de SimpleCalculator"""
    def setUp(self):
        """On setUp un test avec 17 et 5"""
        self.test = SimpleCalculator(17, 5)

    def testAddPositif(self):
        """On test l'addition"""
        result = self.test.addition()
        self.assertEqual(result,22)

    def testSoustraction(self):
        """On test la soustraction"""
        result = self.test.soustraction()
        self.assertEqual(result,12)

    def testProduit(self):
        """On test le produit"""
        result = self.test.produit()
        self.assertEqual(result,85)

    def testQuotient(self):
        """On test le quotient"""
        result = self.test.quotient()
        self.assertEqual(result,3.4)


if __name__ == '__main__':
    unittest.main()
